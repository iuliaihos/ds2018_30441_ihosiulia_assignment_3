package org.consumer.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.consumer.entities.DVD;


public class TextFileService {

	public void printToText(DVD dvd) {
		File file = new File("src/"+dvd.getTitle() + ".txt");
		try {

			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			  
			writer.write("Title " + dvd.getTitle());
			writer.write("\nYear " + dvd.getYear());
			writer.write("\nPrice " + dvd.getPrice());
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
