package org.consumer.start;

import java.util.concurrent.TimeoutException;

import org.consumer.entities.DVD;
import org.consumer.service.TextFileService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

public class TextConsumerStart {
	static final String EXCHANGE_NAME = "dvds";

	public static void main(String[] argv)
			throws java.io.IOException, java.lang.InterruptedException, TimeoutException {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

		String queueName = channel.queueDeclare().getQueue();
	    channel.queueBind(queueName, EXCHANGE_NAME, "");
	    
		System.out.println(" [*] Waiting for messages");

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			String message = new String(delivery.getBody(), "UTF-8");
	        ObjectMapper objectMapper = new ObjectMapper();
			DVD dvd = objectMapper.readValue(message, DVD.class);
	        TextFileService textService = new TextFileService();
	        textService.printToText(dvd);
	        System.out.println(" [x] Received '" + dvd + "'");
		    };
		channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
	}

}
