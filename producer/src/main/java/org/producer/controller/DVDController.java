package org.producer.controller;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.producer.entities.DVD;
import org.producer.services.DVDSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "producer")
public class DVDController {

	@Autowired
	DVDSender sender;

	@PostMapping()
	public String producer(@RequestBody DVD dvd) {
		System.out.println(dvd);
		try {
			sender.send(dvd);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Message sent";
	}

}