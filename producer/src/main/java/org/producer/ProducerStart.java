package org.producer;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"org.producer"})
public class ProducerStart {

    public static void main(String[] args) {
        SpringApplication.run(ProducerStart.class, args);
    }
}
