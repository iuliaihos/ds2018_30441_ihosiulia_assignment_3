package org.producer.services;


import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.producer.entities.DVD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


@Service
public class DVDSender {
	
	static final String EXCHANGE_NAME = "dvds";

	
	@Autowired
	private ConnectionFactory factory;
	
	public void send(DVD dvd) throws IOException, TimeoutException {
	    Connection connection = factory.newConnection();
	    Channel channel = connection.createChannel();
	    channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
	    try {
			 channel.queueDeclare().getQueue();
			 ObjectMapper objectMapper = new ObjectMapper();
			 String message = objectMapper.writeValueAsString(dvd);
			 channel.basicPublish(EXCHANGE_NAME,"", null, message.getBytes("UTF-8"));
			 System.out.println(" [x] Sent '" + message + "'");
			 channel.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
	    connection.close();
	}
}
