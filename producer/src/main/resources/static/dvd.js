function DVDVM(initData) {
    var self = this;
    
    self.urlBase = initData.urlBase;

    self.title = ko.observable();
    self.price = ko.observable();
    self.year = ko.observable();

   self.send = function () {
        var dvd = {
        	title : self.title(), 
            year : self.year(),
            price : self.price()
        }
        console.log(dvd);

        $.ajax({
            type: "POST",
            url: "producer",
            contentType: "application/json; charset=utf-8",
            data : JSON.stringify(dvd),
            success: function (data) {
                swal({
                    title: "The dvd was sent succesfully!",
                    type: "success",
                    onClose: () => {
				     location.reload(); 
				  }
                    });
            },
            error: function(errors) {
            	console.log(errors);
                        swal({
                    title: getResponse(errors.status),
                    type: "error"
                    });         
            }
        });
        
    }

     }
 
function getResponse(error) {
	console.log(error);
	switch(error) {
    case 400:
        return "Bad request"
        break;
    case 200:
        return "Success"
        break;
    default:
        return "Error";
	}
}
     
function RoleViewModel(data) {
	var self = this;
	
	self.id = data.idRole;
	self.name = data.roleName;
}
